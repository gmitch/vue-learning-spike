const app = Vue.createApp({
    data() {
        return {
            courseGoal: 'Learn Vue!',
            courseGoalB: 'Create Apps using Vue',
            vueLink: 'https://vuejs.org/v2/guide/'
        };
    },
    methods: {
        outputGoal() {
            const randomNum = Math.random();
            if (randomNum < 0.5) {
                return this.courseGoal;
            }
            else {
                return this.courseGoalB;
            }
        }
    }
});

app.mount('#user-goal');