const app = Vue.createApp({
  data() {
    return {
      counter: 0,
      name: '',
      confirmedName: ''
    };
  },
  computed: {
    fullName() {
      if (this.name === '') {
        return '';
      }
      return this.name;
    }
  },
  methods: {
    outputFullName() {
      if (this.name === '') {
        return '';
      }
      return this.name;
    },
    resetInput() {
      this.name = '';
    },
    confirmInput() {
      this.confirmedName = this.name;
    },
    setName(event) {
      this.name = event.target.value;
    },
    add(num) {
      this.counter = this.counter + num;
    },
    remove(num) {
      this.counter = this.counter - num;
    }
  }
});

app.mount('#events');
