const app = Vue.createApp({
    data() {
        return {
            selectBoxA: false,
            selectBoxB: false,
            selectBoxC: false,
        };
    },
    methods: {
        boxSelected(box) {
            if (box === 'A') {
                this.selectBoxA = !this.selectBoxA;
            } else if (box === 'B') {
                this.selectBoxB = !this.selectBoxB;
            } else if (box === 'C') {
                this.selectBoxC = !this.selectBoxC;
            }
        }
    },
});